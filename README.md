# Takkan Design

Captures issues which span multiple packages, and general design considerations

## Packages

- [ ] takkan_back4app_client
- [ ] takkan_back4app_generator
- [ ] takkan_backend
- [ ] takkan_client
- [ ] takkan_mason
- [ ] takkan_medley_app
- [ ] takkan_medley_script
- [ ] takkan_schema
- [ ] takkan_script
